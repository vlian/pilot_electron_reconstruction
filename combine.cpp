#include<TFile.h>
#include<TTree.h>
#include<TBranch.h>
#include<TString.h>
int main(int argc,char* argv[]){
	TFile fout(argv[1],"RECREATE");
	for(int i=1;i<7;i++){// i<7
		TFile f(Form("%d.root",i));
		for(int j=0;j<5;j++){
			int idx = 85+5*i+j;
			if(idx>118)continue;
			f.cd();
			TTree *t = (TTree*)f.Get(Form("tree%d",idx));
			int nentries = t->GetEntries();
			printf("%d %d\n",idx,nentries);
			Float_t x,y,z,tx,ty;
			Int_t iz,id;
			t->SetBranchAddress("x",&x);
			t->SetBranchAddress("y",&y);
			t->SetBranchAddress("z",&z);
			t->SetBranchAddress("iz",&iz);
			t->SetBranchAddress("tx",&tx);
			t->SetBranchAddress("ty",&ty);
			t->SetBranchAddress("id",&id);
			fout.cd();
			TTree *tw = new TTree(Form("tree%d",idx),Form("BTs in layer %d",idx));
			tw->Branch("x",&x,"x/F");
			tw->Branch("y",&y,"y/F");
			tw->Branch("z",&z,"z/F");
			tw->Branch("iz",&iz,"iz/I");
			tw->Branch("tx",&tx,"tx/F");
			tw->Branch("ty",&ty,"ty/F");
			tw->Branch("id",&id,"id/I");
			for(int k=0;k<nentries;k++){
				t->GetEntry(k);
				tw->Fill();
			}
			tw->Write();
		}
		f.Close();
	}


	
}
